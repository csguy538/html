function myFunction() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }
  
  function about() {
      document.getElementById("demo").innerHTML = "My name is Dylan Stecklein and I am currently a senior attending Chiawana High School."
  }

  function ctf() {
      document.getElementById("demo").innerHTML = `Here's a program that plays rock, paper, scissors against you. I hear something good happens if you win 5 times in a row.<br>
        Connect to the program with netcat: $ nc saturn.picoctf.net 52473 The program's source code with the flag redacted can be downloaded here. The first thing you need to do is type in 'nc saturn.picoctf.net 52473' into the terminal. You will then type '1' to play the Rock Paper Scissors game. Then all you do is input 'rockpaperscissors into the terminal and you will win the game. Repeat this 5 times and you will get the flag. picoCTF{50M3_3X7R3M3_1UCK_F2114877}`;
  }